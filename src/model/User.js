export default class {
    construct(){
        this.id = '';
        this.login = '';
        this.groups = null;
        this.password = null;
    }

    loadFromDCPM(dcpmUser){
        this.id = dcpmUser.id;
        this.login = dcpmUser.login;
        this.groups = dcpmUser.groups;
    }

    toDCPMData(){
        let User  = Object.assign(
            {login: this.login},
            (this.groups!=null) ? {groups: this.groups.join(',')} : {},
            (this.password!=null) ? {password: this.password} : {}
        );
        return Object.keys(User).map((key)=>{
            return encodeURIComponent(key) + '=' + encodeURIComponent(User[key]);
          }).join('&');        
    }
}