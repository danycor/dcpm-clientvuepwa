export default class {
    construct(){
        this.id = '';
        this.name = '';
        this.desc = '';
        this.dateLastActivity = new Date();
        this.color = 'blue'
    }

    loadFromTrello(trelloProject){
        this.id = trelloProject.id;
        this.name = trelloProject.name;
        this.desc = trelloProject.desc;
        this.dateLastActivity = new Date(Date.parse(trelloProject.dateLastActivity));
        this.color = 'project-'+trelloProject.prefs.background;
    }
    loadFromDCPM(dcpmProject){
        this.id = dcpmProject.id;
        this.name = dcpmProject.name;
        this.desc = dcpmProject.desc;
        this.dateLastActivity = new Date(dcpmProject.dateLastActivity);
        this.color = 'project-'+dcpmProject.color;
    }

    toTrelloData(){
        let project = {
            name: this.name,
            desc: this.desc,
            prefs_background: this.color.replace('project-', '')
        }
        return Object.keys(project).map((key)=>{
            return encodeURIComponent(key) + '=' + encodeURIComponent(project[key].replace(/_/g, '/'));
          }).join('&');
    }
    toDCPMData(){
        let project = {
            name: this.name,
            desc: this.desc,
            color:  this.color.replace('project-', ''),
            dateLastActivity: String(Date.now())
        }
        return Object.keys(project).map((key)=>{
            return encodeURIComponent(key.replace(/_/g, '/')) + '=' + encodeURIComponent(project[key].replace(/_/g, '/'));
          }).join('&');        
    }

    toTrelloDataUpdate(){
        let project = {
            name: this.name,
            desc: this.desc,
            prefs_background: this.color.replace('project-', '')
        }
        return Object.keys(project).map((key)=>{
            return encodeURIComponent(key.replace(/_/g, '/')) + '=' + encodeURIComponent(project[key].replace(/_/g, '/'));
          }).join('&');
    }
}