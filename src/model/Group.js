export default class {
    construct(){
        this.id = '';
        this.name = '';
        this.users = [];
        this.rights = [];
    }

    
    loadFromDCPM(groupDCPM){
        this.id = groupDCPM.id;
        this.name = groupDCPM.name;
        this.users = groupDCPM.users;
        this.rights = groupDCPM.rights;
    }

    toDCPMData(){
        let group = Object.assign(
            {name: this.name},
            (this.users) ? {users: this.users.join(',')} : {},
            (this.rights) ? {rights: this.rights.join(',')} : {}
        )
        return Object.keys(group).map((key)=>{
            return encodeURIComponent(key) + '=' + encodeURIComponent(group[key]);
          }).join('&');        
    }
}