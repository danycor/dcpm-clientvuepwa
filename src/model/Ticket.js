export default class {
    construct(){
        this.id = '';
        this.name = '';
        this.desc = '';
        this.id_list = '';
        this.labels = [];
    }

    loadFromTrello(trelloTicket, list_id){
        this.id = trelloTicket.id;
        this.name = trelloTicket.name;
        this.desc = trelloTicket.desc;
        this.id_list = list_id;
        this.labels = trelloTicket.labels;
    }
    loadFromDCPM(dcpmTicket, list_id){
        this.id = dcpmTicket.id;
        this.name = dcpmTicket.name;
        this.desc = dcpmTicket.desc;
        this.id_list = list_id;
    }

    toTrelloData(){
        let ticket = {
            name: this.name,
            desc: this.desc,
            idList: this.id_list
        }
        return Object.keys(ticket).map((key)=>{
            return encodeURIComponent(key) + '=' + encodeURIComponent(ticket[key].replace(/_/g, '/'));
          }).join('&');
    }
    toDCPMData(){
        let ticket = {
            name: this.name,
            desc: this.desc,
            id_list: this.id_list
        }
        return Object.keys(ticket).map((key)=>{
            return encodeURIComponent(key) + '=' + encodeURIComponent(ticket[key].replace(/_/g, '/'));
          }).join('&');        
    }

    toDCPMDataUpdate(id_list){
        let ticket = {
            id_list: id_list
        }
        return Object.keys(ticket).map((key)=>{
            return encodeURIComponent(key) + '=' + encodeURIComponent(ticket[key]);
        }).join('&'); 
    }
}