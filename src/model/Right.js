export default class {
    construct(){
        this.id = '';
        this.route = '';
        this.methode = '';
    }

    loadFromDCPM(dcpmRight){
        this.id = dcpmRight.id;
        this.route = dcpmRight.route;
        this.methode = dcpmRight.methode;
    }
}