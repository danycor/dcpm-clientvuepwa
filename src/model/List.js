export default class {
    construct(){
        this.id = '';
        this.name = '';
        this.position = '';
        this.id_project = 1;
    }

    loadFromTrello(listTrello){
        this.id = listTrello.id;
        this.name = listTrello.name;
        this.position = listTrello.pos;
        this.id_project = listTrello.idBoard;
    }
    loadFromDCPM(listDCPM){
        this.id = listDCPM.id;
        this.name = listDCPM.name;
        this.position = listDCPM.position;
        this.id_project = listDCPM.id_project;
    }

    toTrelloData(){
        let list = {
            name: this.name,
            pos: this.position,
            idBoard: this.id_project
        }
        return Object.keys(list).map((key)=>{
            return encodeURIComponent(key) + '=' + encodeURIComponent(list[key].replace(/_/g, '/'));
          }).join('&');
    }
    toDCPMData(){
        let list = {
            name: this.name,
            position: this.position,
            id_project:  this.id_project,
        }
        return Object.keys(list).map((key)=>{
            return encodeURIComponent(key) + '=' + encodeURIComponent(list[key].replace(/_/g, '/'));
          }).join('&');        
    }
}