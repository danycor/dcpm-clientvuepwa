import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home.vue'
import Connexion from './views/Connexion.vue'
import Deconnexion from './views/Deconnexion.vue'

import ProjectList from './views/project/ProjectList.vue'
import ProjectCreate from './views/project/ProjectCreate.vue'
import ProjectUpdate from './views/project/ProjectUpdate.vue'

import ListList from './views/list/ListList.vue';
import ListCreate from './views/list/ListCreate.vue';
import ListUpdate from './views/list/ListUpdate.vue';

import TicketList from './views/ticket/TicketList.vue';
import TicketCreate from './views/ticket/TicketCreate.vue';
import TicketUpdate from './views/ticket/TicketUpdate.vue';

import UserList from './views/user/UserList.vue';
import UserCreate from './views/user/UserCreate.vue';
import UserUpdate from './views/user/UserUpdate.vue';

import RightList from './views/right/RightList.vue';

import GroupList from './views/group/GroupList.vue';
import GroupCreate from './views/group/GroupCreate.vue';
import GroupUpdate from './views/group/GroupUpdate.vue';


import NoRights from './views/NoRights.vue'
import NotFind from './views/NotFind.vue'

import User from '@/user.js';

Vue.use(Router)

async function testApiRoute(route, methode){
    return (!User.isConnected()) ? '/connexion' : (await User.haveRight(route, methode) ?  null : '/no-rights');
}

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
        path: '/connexion',
        name: 'connexion',
        component: Connexion,
        props: {
          msg: 'Connexion'
        },
        beforeEnter: (to, from, next) => {
            User.isConnected() ? next('/project-list') : next();
        }
    },
    {
      path: '/deconnexion',
      name: 'deconnexion',
      component: Deconnexion
    },
    {
        path: '/project-list',
        name: 'project-list',
        component: ProjectList,
        beforeEnter:async  (to, from, next) => {
            try {
                let routeToGo = await testApiRoute('project', 'get');
                (routeToGo) ? next(routeToGo) : next();
             } catch(e) {
                next('*');
             }
        }
    },
    {
        path: '/project-create',
        name: 'project-create',
        component: ProjectCreate,
        beforeEnter: (to, from, next) => {
            testApiRoute('project', 'post').then((routeToGo) => {
                (routeToGo) ? next(routeToGo) : next();
            }).catch(() => {
                next('*');
            });
        }
    },
    {
        path: '/project-update',
        name: 'project-update',
        component: ProjectUpdate,
        beforeEnter: (to, from, next) => {
            testApiRoute('project', 'put').then((routeToGo) => {
                (routeToGo) ? next(routeToGo) : next();
            }).catch(() => {
                next('*');
            });
        }
    },
    {
        path: '/list-list',
        name: 'list-list',
        component: ListList,
        beforeEnter: (to, from, next) => {
            testApiRoute('list', 'get').then((routeToGo) => {
                (routeToGo) ? next(routeToGo) : next();
            }).catch(() => {
                next('*');
            });
        }
    },
    {
        path: '/list-create',
        name: 'list-create',
        component: ListCreate,
        beforeEnter: (to, from, next) => {
            testApiRoute('list', 'post').then((routeToGo) => {
                (routeToGo) ? next(routeToGo) : next();
            }).catch(() => {
                next('*');
            });
        }
    },
    {
        path: '/list-update',
        name: 'list-update',
        component: ListUpdate,
        beforeEnter: (to, from, next) => {
            testApiRoute('list', 'put').then((routeToGo) => {
                (routeToGo) ? next(routeToGo) : next();
            }).catch(() => {
                next('*');
            });
        }
    },
    {
        path: '/ticket-list',
        name: 'ticket-list',
        component: TicketList,
        beforeEnter: (to, from, next) => {
            testApiRoute('ticket', 'get').then((routeToGo) => {
                (routeToGo) ? next(routeToGo) : next();
            }).catch(() => {
                next('*');
            });
        }
    },
    {
        path: '/ticket-create',
        name: 'ticket-create',
        component: TicketCreate,
        beforeEnter: (to, from, next) => {
            testApiRoute('ticket', 'post').then((routeToGo) => {
                (routeToGo) ? next(routeToGo) : next();
            }).catch(() => {
                next('*');
            });
        }
    },
    {
        path: '/ticket-update',
        name: 'ticket-update',
        component: TicketUpdate,
        beforeEnter: (to, from, next) => {
            testApiRoute('ticket', 'put').then((routeToGo) => {
                (routeToGo) ? next(routeToGo) : next();
            }).catch(() => {
                next('*');
            });
        }
    },
    {
        path: '/user-list',
        name: 'user-list',
        component: UserList,
        beforeEnter: (to, from, next) => {
            testApiRoute('user', 'get').then((routeToGo) => {
                (routeToGo) ? next(routeToGo) : next();
            }).catch(() => {
                next('*');
            });
        }
    },
    {
        path: '/user-create',
        name: 'user-create',
        component: UserCreate,
        beforeEnter: (to, from, next) => {
            testApiRoute('user', 'post').then((routeToGo) => {
                (routeToGo) ? next(routeToGo) : next();
            }).catch(() => {
                next('*');
            });
        }
    },
    {
        path: '/user-update',
        name: 'user-update',
        component: UserUpdate,
        beforeEnter: (to, from, next) => {
            testApiRoute('user', 'put').then((routeToGo) => {
                (routeToGo) ? next(routeToGo) : next();
            }).catch(() => {
                next('*');
            });
        }
    },
    {
        path: '/group-list',
        name: 'group-list',
        component: GroupList,
        beforeEnter: (to, from, next) => {
            testApiRoute('group', 'get').then((routeToGo) => {
                (routeToGo) ? next(routeToGo) : next();
            }).catch(() => {
                next('*');
            });
        }
    },
    {
        path: '/group-create',
        name: 'group-create',
        component: GroupCreate,
        beforeEnter: (to, from, next) => {
            testApiRoute('group', 'post').then((routeToGo) => {
                (routeToGo) ? next(routeToGo) : next();
            }).catch(() => {
                next('*');
            });
        }
    },
    {
        path: '/group-update',
        name: 'group-update',
        component: GroupUpdate,
        beforeEnter: (to, from, next) => {
            testApiRoute('group', 'put').then((routeToGo) => {
                (routeToGo) ? next(routeToGo) : next();
            }).catch(() => {
                next('*');
            });
        }
    },
    {
        path: '/right-list',
        name: 'right-list',
        component: RightList,
        beforeEnter: (to, from, next) => {
            testApiRoute('right', 'get').then((routeToGo) => {
                (routeToGo) ? next(routeToGo) : next();
            }).catch(() => {
                next('*');
            });
        }
    },
    { 
      path: '/no-rights',
      name: 'no rights',
      component: NoRights
    },
    {
      path: '*',
      name: 'not find',
      component: NotFind
    }
  ]
})