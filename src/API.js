import axios from "axios";

class API {

    /**********************************************************
     *  Project
     **************************************************************/
    /**
     * 
     * @param {*} User 
     */
  async getProjectList(User) {
    let data = {};
    switch (User.getMode()) {
      case "trello":
        data = User.getTrelloData();
        return !data
          ? Promise.reject("No connected")
          : await axios.get("https://api.trello.com/1/members/me/boards/?token=" +data.token +"&key=" +data.key
            );
      case "DCPM":
        data = User.getDCPMData();
        return !data
          ? Promise.reject("No connected")
          : await axios.get("http://" + data.ip + "/projects/?token=" + data.token );
      default:
        return Promise.reject("No right");
    }
  }

    async closeProject(User, project){
        let data = {};
        switch(User.getMode()){
            case 'trello': 
                data = User.getTrelloData();
                return await axios.put('https://api.trello.com/1/boards/'+project.id+'?token='+data.token+'&key='+data.key+'&closed=true')
            case 'DCPM':
                data = User.getDCPMData();
            break;
            default:
                return Promise.reject("Vous n'êtes pas connecté!");
        }
    }

    async deleteProject(User, project){
        let data = {};
        switch(User.getMode()){
            case 'trello': 
                data = User.getTrelloData();
                return await axios.put('https://api.trello.com/1/boards/'+project.id+'?token='+data.token+'&key='+data.key)
            case 'DCPM':
                data = User.getDCPMData();
                return await axios.delete("http://" + data.ip + "/projects/"+project.id+"?token=" + data.token );
            default:
                return Promise.reject("Vous n'êtes pas connecté!");
        }
    }

    async createProject(User, project){
        let Userdata = {};
        switch(User.getMode()){
            case 'trello':
                Userdata = User.getTrelloData();
                return (await axios.post('https://api.trello.com/1/boards/'+ '?token=' + Userdata.token + '&key=' + Userdata.key+'&'+project.toTrelloData())).data
            case 'DCPM':
                Userdata = User.getDCPMData();
                (await axios.post('http://'+Userdata.ip+'/projects/?token='+Userdata.token, project.toDCPMData(), this.getDCPMOption())).data
            break;
            default:
                return Promise.reject("Vous n'êtes pas connecté!");
        }
    }

    async updateProject(User, project){
        let Userdata = {};
        let projectData = {};
        switch(User.getMode()){
            case 'trello':
                Userdata = User.getTrelloData();
                projectData = project.toTrelloDataUpdate();
                return (await axios.put('https://api.trello.com/1/boards/'+project.id+ '?token=' + Userdata.token + '&key=' + Userdata.key+'&'+projectData)).data
            case 'DCPM':
                Userdata = User.getDCPMData();
                (await axios.put('http://'+Userdata.ip+'/projects/'+project.id+'?token='+Userdata.token, project.toDCPMData(), this.getDCPMOption())).data
            break;
            default:
                return Promise.reject("Vous n'êtes pas connecté!");
        }
    }

    /**********************************************************
     *  List
     **************************************************************/
async getList(User, id_project) {
    let data = {};
    switch (User.getMode()) {
        case "trello":
        data = User.getTrelloData();
        return await axios.get("https://api.trello.com/1/boards/"+id_project+"/lists/?token=" +data.token +"&key=" +data.key);
        case "DCPM":
        data = User.getDCPMData();
        return await axios.get('http://'+data.ip+'/lists?token='+data.token+'&id_project='+id_project);
        default:
        return Promise.reject("No right");
    }
}

async createList(User, list){
    let Userdata = {};
    switch(User.getMode()){
        case 'trello':
            Userdata = User.getTrelloData();
            return (await axios.post('https://api.trello.com/1/lists/'+ '?token=' + Userdata.token + '&key=' + Userdata.key+'&'+list.toTrelloData())).data
        case 'DCPM':
            Userdata = User.getDCPMData();
            return (await axios.post('http://'+Userdata.ip+'/lists/?token='+Userdata.token, list.toDCPMData(), this.getDCPMOption())).data
        default:
            return Promise.reject("Vous n'êtes pas connecté!");
    }
}

async updateList(User, list){
    let Userdata = {};
    let listData = {};
    switch(User.getMode()){
        case 'trello':
            Userdata = User.getTrelloData();
            listData = list.toTrelloData();
            return (await axios.put('https://api.trello.com/1/lists/'+list.id+ '?token=' + Userdata.token + '&key=' + Userdata.key+'&'+listData)).data
        case 'DCPM':
            Userdata = User.getDCPMData();
            return (await axios.put('http://'+Userdata.ip+'/lists/'+list.id+'?token='+Userdata.token, list.toDCPMData(), this.getDCPMOption())).data
        default:
            return Promise.reject("Vous n'êtes pas connecté!");
    }
}

async deleteList(User, list){
    let data = {};
    switch(User.getMode()){
        case 'trello': 
            data = User.getTrelloData();
            await axios.post('https://api.trello.com/1/lists/'+list.id+'/archiveAllCards?token='+data.token+'&key='+data.key)
            return await axios.put('https://api.trello.com/1/lists/'+list.id+'/closed?token='+data.token+'&key='+data.key+'&value=true')
        case 'DCPM':
            data = User.getDCPMData();
            return await axios.delete("http://" + data.ip + "/lists/"+list.id+"?token=" + data.token );
        default:
            return Promise.reject("Vous n'êtes pas connecté!");
    }
}
/**********************************************************
 *  tickets
 **************************************************************/
async getTickets(User, list)
{
    let data = {};
    switch(User.getMode()){
        case 'trello': 
            data = User.getTrelloData();
            return await axios.get('https://api.trello.com/1/lists/'+list.id+'/cards?token='+data.token+'&key='+data.key+'&value=true')
        case 'DCPM':
            data = User.getDCPMData();
            return await axios.get("http://" + data.ip + "/tickets/"+"?token=" + data.token+'&id_list='+list.id );
        default:
            return Promise.reject("Vous n'êtes pas connecté!");
    }
}

async createTicket(User, ticket){
    let Userdata = {};
    switch(User.getMode()){
        case 'trello':
            Userdata = User.getTrelloData();
            return (await axios.post('https://api.trello.com/1/cards/'+ '?token=' + Userdata.token + '&key=' + Userdata.key+'&'+ticket.toTrelloData())).data
        case 'DCPM':
            Userdata = User.getDCPMData();
            return (await axios.post('http://'+Userdata.ip+'/tickets/?token='+Userdata.token, ticket.toDCPMData(), this.getDCPMOption())).data
        default:
            return Promise.reject("Vous n'êtes pas connecté!");
    }
}

async moveTicket(User, ticket, newList){
    let Userdata = {};
    switch(User.getMode()){
        case 'trello':
            Userdata = User.getTrelloData();
            return (await axios.put('https://api.trello.com/1/cards/'+ticket.id+'/?token=' + Userdata.token + '&key=' + Userdata.key+'&idList='+newList.id)).data
        case 'DCPM':
            Userdata = User.getDCPMData();
            return (await axios.put('http://'+Userdata.ip+'/tickets/'+ticket.id+'/?token='+Userdata.token, ticket.toDCPMDataUpdate(newList.id), this.getDCPMOption())).data
        default:
            return Promise.reject("Vous n'êtes pas connecté!");
    }
}

async deleteTicket(User, ticket){
    let data = {};
    switch(User.getMode()){
        case 'trello': 
            data = User.getTrelloData();
            return await axios.delete('https://api.trello.com/1/cards/'+ticket.id+'/?token='+data.token+'&key='+data.key)
        case 'DCPM':
            data = User.getDCPMData();
            return await axios.delete("http://" + data.ip + "/tickets/"+ticket.id+"?token=" + data.token );
        default:
            return Promise.reject("Vous n'êtes pas connecté!");
    }
}

async updateTicket(User, ticket){
    let Userdata = {};
    let ticketData = {};
    switch(User.getMode()){
        case 'trello':
            Userdata = User.getTrelloData();
            ticketData = ticket.toTrelloData();
            return (await axios.put('https://api.trello.com/1/cards/'+ticket.id+ '?token=' + Userdata.token + '&key=' + Userdata.key+'&'+ticketData)).data
        case 'DCPM':
            Userdata = User.getDCPMData();
            return (await axios.put('http://'+Userdata.ip+'/tickets/'+ticket.id+'?token='+Userdata.token, ticket.toDCPMData(), this.getDCPMOption())).data
        default:
            return Promise.reject("Vous n'êtes pas connecté!");
    }
}

/**********************************************************
 *  User
 **************************************************************/
async getUsers(User)
{
    let data = {};
    switch(User.getMode()){
        case 'DCPM':
            data = User.getDCPMData();
            return await axios.get("http://" + data.ip + "/users/"+"?token=" + data.token);
        default:
            return Promise.reject("Vous n'êtes pas connecté!");
    }
}

async createUser(User, user){
    let Userdata = {};
    switch(User.getMode()){
        case 'DCPM':
            Userdata = User.getDCPMData();
            return (await axios.post('http://'+Userdata.ip+'/users/?token='+Userdata.token, user.toDCPMData(), this.getDCPMOption())).data
        default:
            return Promise.reject("Vous n'êtes pas connecté!");
    }
}

async deleteUser(User, user){
    let data = {};
    switch(User.getMode()){
        case 'DCPM':
            data = User.getDCPMData();
            return await axios.delete("http://" + data.ip + "/users/"+user.id+"?token=" + data.token );
        default:
            return Promise.reject("Vous n'êtes pas connecté!");
    }
}

async updateUser(User, user){
    let Userdata = {};
    switch(User.getMode()){
        case 'DCPM':
            Userdata = User.getDCPMData();
            return (await axios.put('http://'+Userdata.ip+'/users/'+user.id+'?token='+Userdata.token, user.toDCPMData(), this.getDCPMOption()))
        default:
            return Promise.reject("Vous n'êtes pas connecté!");
    }
}

/**********************************************************
 *  Groups
 **************************************************************/
async getGroups(User)
{
    let data = {};
    switch(User.getMode()){
        case 'DCPM':
            data = User.getDCPMData();
            return await axios.get("http://" + data.ip + "/groups/"+"?token=" + data.token);
        default:
            return Promise.reject("Vous n'êtes pas connecté!");
    }
}

async createGroup(User, group){
    let Userdata = {};
    switch(User.getMode()){
        case 'DCPM':
            Userdata = User.getDCPMData();
            return (await axios.post('http://'+Userdata.ip+'/groups/?token='+Userdata.token, group.toDCPMData(), this.getDCPMOption())).data
        default:
            return Promise.reject("Vous n'êtes pas connecté!");
    }
}

async deleteGroup(User, group){
    let data = {};
    switch(User.getMode()){
        case 'DCPM':
            data = User.getDCPMData();
            return await axios.delete("http://" + data.ip + "/groups/"+group.id+"?token=" + data.token );
        default:
            return Promise.reject("Vous n'êtes pas connecté!");
    }
}

async updateGroup(User, group){
    let Userdata = {};
    switch(User.getMode()){
        case 'DCPM':
            Userdata = User.getDCPMData();
            return (await axios.put('http://'+Userdata.ip+'/groups/'+group.id+'?token='+Userdata.token, group.toDCPMData(), this.getDCPMOption()))
        default:
            return Promise.reject("Vous n'êtes pas connecté!");
    }
}

/**********************************************************
 *  Rights
 **************************************************************/
async getRights(User)
{
    let data = {};
    switch(User.getMode()){
        case 'DCPM':
            data = User.getDCPMData();
            return await axios.get("http://" + data.ip + "/rights/"+"?token=" + data.token);
        default:
            return Promise.reject("Vous n'êtes pas connecté!");
    }
}

    /**********************************************************
     *  Other
     **************************************************************/
  /**
   * 
   * @param {*} user 
   */
  async getConnexion(user) {
    switch(user.mode) {
        case 'trello': return await axios.get("https://api.trello.com/1/members/me/boards?key="+user.key+"&token="+user.token);
        case 'DCPM': return await axios.post("http://"+user.ip+'/login', this.getDCPMParameters({login: user.login, password: user.password}), this.getDCPMOption());
        default: return Promise.reject('Cannot connect!');
    }
  }

  getDCPMOption(){
    return {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
  }

  getDCPMParameters(object){
    return Object.keys(object).map((key) => {
        return key+'='+object[key];
    }).join('&');

  }
}

export default new API();
