import axios from "axios";

class User {
    constructor() {
        this.mode = null;
        this.token = null;
        this.key = null;
        this.ip = null;
        this.rights = null;
    }

    saveData(datas) {
        Object.keys(datas).forEach(key => {
            window.localStorage.setItem(key, JSON.stringify(datas[key]));
        });
    }

    getMode() {
        return  (this.mode = JSON.parse(window.localStorage.getItem("mode")));
    }

    getTrelloData() {
        return this.getMode() == "trello"
        ? this.key != null && this.token != null
            ? { key: this.key, token: this.token }
            : {
                key: (this.key = JSON.parse(window.localStorage.getItem("trellokey"))),
                token: (this.token = JSON.parse(window.localStorage.getItem("token")))
            }
        : null;
    }

    getDCPMData() {
        return this.getMode() == "DCPM"
        ? this.token != null && this.ip != null
            ? { ip: this.ip, token: this.token }
            : {
                ip: (this.ip = JSON.parse(window.localStorage.getItem("ip"))),
                token: (this.token = JSON.parse(window.localStorage.getItem("token")))
            }
        : null;
    }

    getTrelloRights() {
        return new Promise((resolve) => {
            this.rights = {
                project: {get: true, put: true, post: true, delete: false},
                list: {get: true, put: true, post: true, delete: true},
                ticket: {get: true, put: true, post: true, delete: true},
                group: {get: false, put: false, post: false, delete: false},
                user: {get: false, put: false, post: false, delete: false},
                right: {get: false, put: false, post: false, delete: false}
            }
            resolve(this.rights);
        })
    }

    getDCPMRights() {
        this.getDCPMData();
        return new Promise((resolve) => {
            if(this.rights !=null){
                resolve(this.rights);
            }else {
                let rightObject = this.getTempRight();
                axios.get("http://"+this.ip+'/'+'?token='+this.token).then((r) => {
                    r.data.forEach((right) => {
                        let methode = right.name.split("/")[0];
                        let object = right.name.split("/")[1].substr(0, right.name.split("/")[1].length-1)
                        if(rightObject[object] && rightObject[object][methode]){
                            rightObject[object][methode] = true;
                        }
                        this.rights = rightObject;
                        resolve(rightObject)
                    });
                })
            }
        });
    }

    async getRights() {
        switch(this.getMode()){
            case 'trello': return this.rights ? this.rights : await this.getTrelloRights();
            case 'DCPM': return this.rights ? this.rights : await this.getDCPMRights();
            default: return this.getTempRight();
        }
    }
    getTempRight(){
        return {
            project: {get: true, put: true, post: true, delete: true},
            list: {get: true, put: true, post: true, delete: true},
            ticket: {get: true, put: true, post: true, delete: true},
            group: {get: true, put: true, post: true, delete: true},
            user: {get: true, put: true, post: true, delete: true},
            right: {get: true, put: true, post: true, delete: true}
        }
    }
    async haveRight(route, methode){
        return (await this.getRights())[route][methode];
    }

    isConnected() {
        return this.getMode() ? true : false;
    }

}

export default new User();
